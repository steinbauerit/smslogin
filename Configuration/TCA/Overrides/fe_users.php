<?php
/**
 * Created by PhpStorm.
 * User: hoja
 * Date: 19/7/16
 * Time: 12:56 AM
 */
$tmp_columns = array(
    'tx_smslogin_otptoken' => array(
        'exclude' => 1,
        'label' => 'OTP Token',
        'config' => array(
            'type' => 'text',
            'readOnly' => '1'
        ),
    ),
    'tx_smslogin_otpstatus' => array(
        'exclude' => 1,
        'label' => 'OTP Status',
        'config' => array(
            'type' => 'text',
            'readOnly' => '1'
        ),
    )
);

#New Type For Extended FE User
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tmp_columns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', '--div--;LLL:EXT:smslogin/Resources/Private/Language/locallang_db.xlf:fe_users.tabs.tx_smslogin_felogin, tx_smslogin_otptoken,tx_smslogin_otpstatus', '', '');