<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'SteinbauerIT.' . $_EXTKEY,
    'Smsfelogin',
    array(
        'Login' => 'login, logout, forgotpassword, confirmotp, success, changepassword',
        
    ),
    // non-cacheable actions
    array(
        'Login' => 'login, logout, forgotpassword, confirmotp, success, changepassword',
        
    )
);