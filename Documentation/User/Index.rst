﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _user-manual:

Users Manual
============

The extension is a frontend plugin that allows userlogin. This extension has the feature to double secure every login using a sms OTP.

Add the plugin configurations
:::::::::::::::::::::::::::::

Please see the configuration page:

.. figure:: ../Images/UserManual/BackendView.png
   :width: 700px
   :alt: Bakend view

   Default Backend view

   The Backend view of TYPO3 after thefuser insert the plugin as content element


The fontend Login Form
:::::::::::::::::::::::::

The form to fill out the Username and Password. You can enter the User's userid or email in the user field.



.. figure:: ../Images/UserManual/loginForm.png
   :width: 500px
   :alt: Login Form

   Frontend login form


Form to enter OTP
:::::::::::::::::

In the form below, you have to enter the OTP recieved in your given mobile number or email id: This ensures security.


.. figure:: ../Images/UserManual/otp.png
   :width: 500px
   :alt: enter otp

   Form to enter OTP


Forgot Password
::::::::::::::::

The plugin also have option to handle forgot password. The below form appears to enter the user's Email id to which a link will be send to change the password.


.. figure:: ../Images/UserManual/forgotPass.png
   :width: 500px
   :alt: forgotpassword form

   Forgot password form


.. figure:: ../Images/UserManual/mailSent.png
   :width: 500px
   :alt: email send


The link follows to another form where you have to enter the new login password.


.. figure:: ../Images/UserManual/newPass.png
   :width: 500px
   :alt: new password form

   Change password form
