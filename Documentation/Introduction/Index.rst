﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _introduction:

Introduction
============


.. _what-it-does:

What does it do?
----------------

The smslogin extension is a frontend plugin that allows two-way authentication for frontend user login. It uses SMS OTP to authenticate the users. OTP is send to the phone number mentioned in the website user record details.
Only if the entered OTP is correct the login is successful.


Features
:::::::::

* OTP can be sent to user's email id if enabled in plugin options.