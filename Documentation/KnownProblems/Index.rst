﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _known-problems:

Known Problems
==============

Currently no problems are reported.
Please report to
`bug tracker <http://forge.typo3.org/projects/typo3cms-doc-official-extension-template/issues>`_



